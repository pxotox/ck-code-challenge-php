import axios from "axios";
import { shallowMount } from '@vue/test-utils'
import TodoList from '@/components/TodoList.vue'

const result = []

axios.get = () => {
  return Promise.resolve({
    data: { result: result }
  })
}


describe('TodoList.vue', () => {
  it('should render', () => {
    const wrapper = shallowMount(TodoList)
    expect(wrapper.contains('.todo')).toBe(true)
  })

  it('should compute remaining tasks count', () => {
    const wrapper = shallowMount(TodoList)
    expect(wrapper.vm.remainingTaskCount).toBe(0)
    expect(wrapper.find('.todo__footer span').text()).toBe('Nothing to do!')
    wrapper.vm.items.push({'id': 1})
    expect(wrapper.vm.remainingTaskCount).toBe(1)
    expect(wrapper.find('.todo__footer span').text()).toBe('1 tasks to do')
  })

  it('should load list when created', async () => {
    const wrapper = shallowMount(TodoList)
    expect(wrapper.vm.isLoading).toBe(true)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.isLoading).toBe(false)
    expect(wrapper.vm.items).toBe(result)
  })


  it('should load list when created', () => {
    const wrapper = shallowMount(TodoList)
    wrapper.vm.isAddingTask = false
    wrapper.vm.isLoadingList = false
    expect(wrapper.vm.isLoading).toBe(false)

    wrapper.vm.isAddingTask = true
    wrapper.vm.isLoadingList = false
    expect(wrapper.vm.isLoading).toBe(true)

    wrapper.vm.isAddingTask = false
    wrapper.vm.isLoadingList = true
    expect(wrapper.vm.isLoading).toBe(true)

    wrapper.vm.isAddingTask = true
    wrapper.vm.isLoadingList = true
    expect(wrapper.vm.isLoading).toBe(true)
  })

  it('should add a task', async () => {
    const task = {'id': 1, 'title': 'Task #1', 'completed': false}
    axios.post = () => {
      console.log('post')
      return Promise.resolve({
        data: task
      })
    }

    const wrapper = shallowMount(TodoList)
    wrapper.vm.taskTitle = task.title
    wrapper.vm.addTask()
    expect(wrapper.vm.items.length).toBe(0)
    expect(wrapper.vm.isAddingTask).toBe(true)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.items.length).toBe(1)
    expect(wrapper.vm.items).toEqual([task])
    expect(wrapper.vm.isAddingTask).toBe(false)
  })
})
