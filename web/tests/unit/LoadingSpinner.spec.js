import { shallowMount } from '@vue/test-utils'
import LoadingSpinner from '@/components/LoadingSpinner.vue'

const wrapper = shallowMount(LoadingSpinner)

describe('LoadingSpinner.vue', () => {
  it('should render', () => {
    expect(wrapper.contains('.loading')).toBe(true)
  })
})
