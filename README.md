At CrossKnolwedge we build scalable applications that enable learners to have the best learning experience while acquiring the skills they need to be successful on their daily jobs.

We use what we think are the right tools to build our applications and we are not afraid of experimenting new technologies.

To enable this discovery our developers need to be up-to-date with new technologies and need to be able to quickly learn and adapt to the fast-changing technological environment we have today.

On this code challenge we will use a variety of technologies to build a Single-Page application and a REST API.


> **Notes:**
> You don't need to already know or be an expert on all the technologies that are used here.
> Everything is already set up and ready to use, however it is good to have an overview of each technoligy to understand why they are being used.

# Technologies overview

## Frontend
Here are some frontend technologies that we currently have in our stack and we want you to use on this code challenge.

### Vue.js
Vue is a progressive framework for building user interfaces. It focus on on the view layer and its a powerfull tool to build Single-Page applications (SPA) with a very narrow learning courve. It has powerfull builtin tools to integreate with other technologies as SCSCC and ECMAScript.
We use Vue.js to quickly create SPAs for our learning experiences as it helps on the componentization of the applications and reusability.

Documentation: [Vue.js](https://vuejs.org/v2/guide/)

### Axios
Axio is a promise based HTTP client for the browser and node.js. Its a easy to use component that has a great adoption specially when using Vue.js.
We use it for all asynchronous HTTP calls we need to perform.

Documentation: [Axios](https://github.com/axios/axios)

### Jest
Jest is a consolidated JavaScript testing solution. We use it to test all components we build using Vue.js.

Documentation: [Jest - Getting Started](https://jestjs.io/docs/en/getting-started.html)

## Backend
To build REST APIs and complex web applications we use Python with Django, PHP with Synfony and .NET core. For this challange we will focus on PHP and Symfony.

### PHP
PHP is a popular general-purpose scripting language that is especially suited to web development.

Documentation: [Getting started](http://php.net/manual/en/getting-started.php)

#### Symfony
Symfony is a set of reusable PHP components and a PHP framework for web projects.

Documentation: [Symfony 3.4 Docs](http://php.net/manual/en/getting-started.php)

## Docker and Docker Compose
Docker is a computer program that performs operating-system-level virtualization, also known as "containerization". It makes it easy to create contained systems that can be easily set up, reused and updated.
Docker Compose is a tool for defining and running multi-container Docker applications. It enables to orchestrate all dependencies using Docker containers.
We use Docker and Docker Compose on our development environment to be able to quickly change from one project to another without having to setup multiple versions of applications and to make sure our development environment is the closest to production we can.

Documentation: [Docker - Get Started](https://docs.docker.com/get-started/) and [Docker Compose - Get Started](https://docs.docker.com/compose/gettingstarted/)

----

# Code challenge

During this code challenge we want you to apply all your knowledge on the best practices and standards on software development to enhance an application that has already been started.

You will have 5 use cases to implement and 2 other extra use cases if you want to go the extra mile.

Also, you can send your comments on what you would do differently while build an application like this.

**[ALL USE CASES ARE AVAILABLE CLICKING HERE](USE_CASES.md)**

## The application

The application you are going to work on is a simple todo list implemented as a Single-Page application using Vue.js that connects to a REST API built with PHP and Symfony.

![UC001 Mockup](https://i.ibb.co/4jqqh6R/app.png)

---

## Implementation

Now let's go to the real interesting part, here is how you can start to write some code.

### Requirements
To be able to run this project you'll need to have the following requirements met:

**With Docker**

* Git - [All platforms](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* Docker - [Ubuntu](https://docs.docker.com/install/) - [macOS](https://docs.docker.com/docker-for-mac/install/) - [Windows](https://docs.docker.com/docker-for-windows/install/) - [Other platforms](https://docs.docker.com/install/)
* Docker Compose - [All platforms](https://docs.docker.com/compose/install/)
* 3gb of free disk space (after installing the other requirements)

**Without Docker**

* Git - [All platforms](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* PHP - [http://php.net/downloads.php](http://php.net/downloads.php)
* Composer - [https://getcomposer.org/download/](https://getcomposer.org/download/)
* 3gb of free disk space (after installing the other requirements)

### Quickstart

To have a quickstart and get everything running read the following READMEs depending on your tools of choice:

* If you are going to **use Docker** check [this README file](DOCKER.md).
* If you are **not using Docker** check these README files: ([API README](api/README.md) and [front-end README](web/README.md)).

### Guidelines
We have some guidelines to be followed when possible:

1. Everything must be in English
2. Write unit tests for you code
3. Use the following format for commit messages: `[UCXXX] Commit message`
4. Follow the code standards already in place
5. You can change everything you think need to be changed, there is no limitation

### File structure
Below is the file structure of the project with the main files you will need.

```
├── api # REST API
│   └─── app
│       ├── config # Configuration folder
│       ├── bin # Binaries folder
│       ├── src
│       │   └── AppBundle
│       │       ├── Controller
│       │       │   ├── ApiController.php
│       │       │   └── DefaultController.php
│       │       ├── Entity
│       │       │   └── Task.php
│       │       ├── Form
│       │       │   └── TaskType.php
│       │       ├── Repository
│       │       │   └── TaskRepository.php
│       │       └── Service
│       │           └── Serializer.php
│       └── tests
│           └── AppBundle
│               ├── Controller
│                   ├── ApiControllerTest.php
│                   └── DefaultControllerTest.php
│       └── web # Public files folder
└── web # SPA built with Vue.js
    ├─── src
    │    ├── components
    │    │   ├── Loading.vue
    │    │   └── TodoList.vue
    │    ├── scss
    │    │   └── app.scss
    │    └── App.vue
    └─── tests
         └─── unit
```

----

Feel free to ask us any questions that you might have during the challenge. We sincerely hope you have a good time while doing it.

**Good luck and have fun!**