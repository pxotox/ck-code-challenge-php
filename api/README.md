# API (Back-end)
This part of the project implements the REST API used by the front-end to retrieve and save data.

# Requirements
To be able to run this part of the project you'll need to have the following requirements met:

* PHP7+
* Composer

# Quickstart

Here is a quick start to get everything running, you should be able just to copy and paste the following commands and have everything set up.

## Running the application

```
git clone git@bitbucket.org:pxotox/ck-code-challenge-php.git
cd ck-code-challenge-php/api/
composer install
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console server:run 0.0.0.0:8000
chmod -R +777 /app/var/
```

## Acessing the application

After running the application you'll be able to access it here: [http://localhost:8000/api/v1/tasks](http://localhost:8000/api/v1/tasks).

## Running tests

To run PHPUnit tests you can execute this command: `./vendor/bin/simple-phpunit`.

## Updating database schema

If you edit any model (inside `src/AppBundle/Entity/`) and need to update the database schema you can execute this command: `php bin/console doctrine:schema:update --force`.

> **Notes:**
> We choose not to use migrations for this project.
